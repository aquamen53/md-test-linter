# Тестирование

![logo](https://prognote.ru/wp-content/uploads/2021/11/software-g93e339fa4_1920.jpg "title image")

## Оглавление

[[_TOC_]]

## Введение

Тестирование является важной составляющей разработки программного обеспечения. Мы проводим тестирование непрерывно,
осознаем мы это или нет. Например, когда мы пишем класс на языке PHP, мы можем отлаживать его шаг за шагом или просто
использовать echo или die для проверки, что реализация работает в соответствии с намеченным планом. В случае веб
приложения, мы вводим некоторые тестовые данные в форму для того, чтобы убедиться, что страница взаимодействует с нами,
как ожидается.

Процесс тестирования может быть автоматизирован так, что каждый раз, когда нам нужно что-то проверить, мы просто должны
вызвать код, который сделает это за нас. Код, который проверяет, что результат совпадает с тем, что мы планировали,
называется тестом, а процесс создания тестов и их последующего использования - автоматизированным тестированием, что и
является главной темой данного раздела.

### В данной статье мы разберем фреймворк для тестирования Codeception вместе с YII2.

Codeception — это популярный фреймворк для тестирования веб-приложений. Он написан поверх PHPUnit и позволяет более
элегантно писать тесты используя методологию BDD(Behavior-driven development). Поддерживает следующие виды
тестирования:</p>

* Unit tests (модульные тесты) — тестирование отдельных, изолированных php-классов.

* Integration tests (интеграционные тесты) - тестирование в связке с базами данных и другими компонентами приложения

* Functional tests (функциональные тесты) — тестирование приложения без использования веб-сервера и без поддержки
  javascript.

* Acceptance tests (приёмочные тесты) — тестирование с использованием браузера, имитирует действия пользователя,
  поддерживает javascript. Этот вид тестирования интересует нас в первую очередь.

* Api tests - тестирование апи и soap сервисов

* Smoke tests (дымчатые тесты) - проверка работы сервиса без углубления

## 1. Общие требования

---
Потребуются следующие extension:

```json
{
  "require-dev": {
    "codeception/module-phpbrowser": "^1.0.2",
    "codeception/module-rest": "^2.0",
    "codeception/specify": "^2.0"
  }
}
```

### Инициализация codeception

```bash
vendor/bin/codecept bootstrap vendor/migcredit/_название_сервиса_/src/ -s migcredit\\_НазваниеСервиса_\\src\\tests
```

### Создание сюитов

* `vendor/bin/codecept g:suite api ApiTester -c vendor/migcredit/_название_сервиса_/src/ -> api + ApiTester`
* `vendor/bin/codecept g:suite integration CodeTester -c vendor/migcredit/_название_сервиса_/src/ -> integration + CodeTester`
* `vendor/bin/codecept g:suite unit UnitTester -c vendor/migcredit/_название_сервиса_/src/ -> unit + UnitTester`
* `vendor/bin/codecept g:suite acceptance AcceptanceTester -c vendor/migcredit/_название_сервиса_/src/ -> acceptance + AcceptanceTester`

### Создание тестов

* `vendor/bin/codecept g:test unit Example -c vendor/migcredit/_название_сервиса_/src/`
* `vendor/bin/codecept g:cest suite Login -c vendor/migcredit/_название_сервиса_/src/`

[Список всех команд](https://codeception.com/docs/reference/Commands)

### Конфиг Codeception

```yaml
    bootstrap: _bootstrap.php
    settings:
      memory_limit: 1024M
      colors: true
      modules:
        enabled:
          - Asserts
          - Yii2
        config:
          Yii2:
            configFile: "/app/frontend/config/test.php"
            cleanup: false
            transaction: false
```

Необходимо создать файл `_bootstrap.php` по пути `vendor/migcredit/_название_сервиса_/src/tests/` со следующим
содержанием:

```php
define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);

// Search for autoload, since performance is irrelevant and usability isn't!
$dir = __DIR__ . '/..';
while (!file_exists($dir . '/app/vendor/autoload.php')) {
    if ($dir == dirname($dir)) {
        throw new \Exception('Failed to locate autoload.php');
    }
    $dir = dirname($dir);
}

$vendor = $dir . '/app/vendor';

define('VENDOR_DIR', $vendor);

require_once $vendor . '/autoload.php';
require $vendor . '/yiisoft/yii2/Yii.php';
```

После каждого изменения конфига необходимо выполнить команду:

```bash
vendor/bin/codecept build -c vendor/migcredit/_название_сервиса_/src/
```

[Статья по интеграции с yii2](https://codeception.com/for/yii)

## 2. Модульное тестирование

Модульное тестирование это тип тестирования программного обеспечения, при котором тестируются отдельные модули или
компоненты программного
обеспечения. Его цель заключается в том, чтобы проверить, что каждая единица программного кода работает должным образом.
Данный вид тестирование выполняется разработчиками на этапе кодирования приложения. Модульные тесты изолируют часть кода
и проверяют его работоспособность. Единицей для измерения может служить отдельная функция, метод, процедура, модуль или
объект.

Данный тест проверяет валидацию в классе Юзер.

```php
namespace Tests\Unit;

use \Tests\Support\UnitTester;

class UserTest extends \Codeception\Test\Unit
{
    public function testValidation()
    {
        $user = new \App\User();

        $user->setName(null);
        $this->assertFalse($user->validate(['username']));

        $user->setName('toolooongnaaaaamee');
        $this->assertFalse($user->validate(['username']));

        $user->setName('davert');
        $this->assertTrue($user->validate(['username']));
    }
}
```

Описание основных команд

```php
$this->assertFalse(flase); //Проверить выражение на ложь
$this->assertTrue(true); //Проверить выражение на правду
$this->assertEquals(1,1); //Проверить что две переменные равны
$this->assertNotEquals()(1,12); //Проверить что две переменные не равны
$this->assertContains(4,[10,4,5]); //Проверить массив на содержание определенного элемента
$this->assertNotContains(6,[10,4,5]); //Проверить массив не содержит определенный элемент
$this->assertNull(null); //Проверить выражение на null
$this->assertNotNull('not null'); //Проверить выражение на неравенство null
$this->assertEmpty(''); //Проверить выражение на empty
$this->assertNotEmpty('not empty'); //Проверить выражение на не empty
```

Тесты можно делать зависимыми через PHPDOC. Достаточно указать @depends и название метода. После этого выполнится
сначала тот что был указан в depends, а затем основной.
Так же можно отдавать результат зависимого метода в качестве параметров в основной метод. Для этого в основном
необходимо указать входные параметры:

```php
use PHPUnit\Framework\TestCase;

class MultipleDependenciesTest extends TestCase
{
    public function testProducerFirst()
    {
        $this->assertTrue(true);
        return 'first';
    }

    public function testProducerSecond()
    {
        $this->assertTrue(true);
        return 'second';
    }

    /**
     * @depends testProducerFirst
     * @depends testProducerSecond
     */
    public function testConsumer($a, $b)
    {
        $this->assertSame('first', $a);
        $this->assertSame('second', $b);
    }
}
```

[Про UNIT и интеграционное тестирование](https://codeception.com/docs/UnitTests)

## 3. Интеграционное тестирование

Интеграционное тестирование – это тип тестирования, при котором программные модули объединяются логически и тестируются
как группа. Как правило, программный продукт состоит из нескольких программных модулей, написанных разными
программистами. Целью тестирования является выявление багов при взаимодействии между этими программными модулями
и в первую очередь направлен на проверку обмена данными между этими самими модулями.

Для того что бы использовать ORM в работе тестов необходимо поправить конфиг Unit тестера:

```yaml
actor: UnitTester
modules:
  enabled:
    - Asserts
    - Yii2:
        part: ORM
```

И написать такой простой тест сохранения данных в бд:

```php
function testUserNameCanBeChanged()
{
    $id = $this->tester->haveRecord('users', ['name' => 'miles']);
    $user = User::find($id);
    $user->setName('bill');
    $user->save();
    $this->assertEquals('bill', $user->getName());
    $this->tester->seeRecord('users', ['name' => 'bill']);
    $this->tester->dontSeeRecord('users', ['name' => 'miles']);
}
```

Интеграционные тесты поддерживают те же выражения, что и юнит, но со своими дополнениями.

Описание команд используемых в связке с yii2

```php
$this->tester->haveRecord(); // создает запись в таблице и удаляет её после теста
$this->tester->seeRecord(); // найти запись в таблице
$this->tester->dontSeeRecord(); // убедится что записи нет в таблице
$this->tester->seeEmailIsSent(); // проверить что имейл был отправлен
$this->tester->grabLastSentEmail(); // получить последний отправленный имейл
$this->tester->haveFixtures(); // загрузить фикстуру
$this->tester->grabFixture(); // получить данные из коллекции с фикстурами
```

## 4. Функциональное тестирование

Как уже было сказано в описании это тестирование приложения без использования сервера, но с использованием механизмов
как при тестировании с сервером, т.е. используются те же команды, что и при приемочном тестировании.

Функциональные тесты, как правило, намного быстрее, чем приемочные тесты. Но функциональные тесты менее стабильны, так
как они запускают Codeception и приложение в одной среде.

Функциональные тесты не могут получить доступ к внешним URL-адресам, только к URL-адресам в вашем проекте. Вы можете
использовать PhpBrowser для открытия внешних URL-адресов.

В функциональном тестировании, в отличие от запуска приложения традиционным способом, PHP-приложение не останавливается
после завершения обработки запроса. Поскольку все запросы выполняются в одном контейнере памяти, они не изолированы.
Поэтому, если вы видите, что ваши тесты таинственным образом терпят неудачу, когда они не должны - попробуйте выполнить
один тест. Это покажет, не прошли ли тесты, потому что они не были изолированы во время запуска. Держите память в
чистоте, избегайте утечек памяти и очищайте глобальные и статические переменные.

```php
$I->amOnPage(['site/contact']);
$I->click('Logout');
$I->click('Logout', '.nav');
$I->click('a.logout');
$I->click(['class' => 'logout']);
$I->submitForm('form#login', ['name' => 'john', 'password' => '123456']);
$I->fillField('#login input[name=name]', 'john');
$I->fillField('#login input[name=password]', '123456');
$I->click('Submit', '#login');
$I->see('Welcome, john');
$I->see('Logged in successfully', '.notice');
$I->seeCurrentUrlEquals('/profile/john');
```

Описание основных команд

```php
$I->amOnPage(); // перейти на url
$I->click(); // осуществить клик по элементу/тексту
$I->submitForm(); // отправить форму с данными
$I->fillField(); // заполнить поле формы
$I->see(); // найти в области видимости элемент/текст
$I->seeCurrentUrlEquals(); // проверить содержит ли url адрес определенный путь
```

[Про функциональное тестирование](https://codeception.com/docs/FunctionalTests)

### 4.1 Тестирование апи

Тестирование апи поможет проверить ваш веб сервис

Тесты API не включены ни в какие шаблоны Yii, поэтому вам нужно настроить их вручную, если вы разрабатываете веб-сервис.
Тестирование API проводится на уровне функционального тестирования, но вместо того, чтобы тестировать HTML-ответы на
действия пользователя, они тестируют запросы и ответы с помощью таких протоколов, как REST или SOAP.

Нужно создать новый сюит (есть в общих требованиях) апи и дополнить его конфигурацию:

```yaml
class_name: ApiTester
modules:
  enabled:
    - REST:
        depends: Yii2
        part: [ json ]
        url: ''
    - Yii2:
        - part: [ orm, fixtures ]
    - Phiremock:
        host: 127.0.0.1
        port: 8081
        resetBeforeEachTest: false
    - SOAP:
        depends: PhpBrowser
        endpoint: http://serviceapp/api/v1/
  config:
    - Yii2:
        entryScript: /app/frontend/web/index-test.php
```

Действия модуля Yii2, такие, как amOnPage или see, не должны быть доступны для тестирования API. Вот почему модуль Yii2
не включен, но объявлен в зависимости от модуля REST.

Пример теста REST апи

```php
$I->haveHttpHeader('Content-Type', 'application/json');
$I->haveHttpHeader('Process', LoginFormDictionary::LF_PROCESS_AUTHORIZATION);
        
$I->seeRecord(AgentProfile::class, ['Phone' => $this->login]);

$I->sendPost('/AuthorizeUser', [
    "Login" => $params['Login'],
    "Password" => $params['Password'],
    "TargetSystem" => $params['TargetSystem'],
    "MessageInfo" => $params['MessageInfo']
]);

$I->seeResponseCodeIsSuccessful();
$I->seeResponseIsJson();

$I->seeResponseContainsJson([
    'BaseResponse' => [
        'ResponseCode' => 0,
        'ResponseMessage' => 'Success'
    ]
]);
```

Пример теста SOAP

```php
$I->sendGet('/users.xml');
$I->seeResponseCodeIs(\Codeception\Util\HttpCode::OK); // 200
$I->seeResponseIsXml();
$I->seeXmlResponseMatchesXpath('//user/login');
$I->seeXmlResponseIncludes(\Codeception\Util\Xml::toXml([
    'user' => [
      'name' => 'davert',
      'email' => 'davert@codeception.com',
      'status' => 'inactive'
  ]
]));
```

Описание основных команд

```php
$I->sendPost(); // отправить пост запрос
$I->sendGet(); // отправить гет запрос

$I->haveHttpHeader(); //Отравленный запрос будет содержать следующие заголовки
$I->seeResponseCodeIsSuccessful(); // проверить что отправленный запрос получил 200 статус
$I->seeResponseIsJson(); // проверить что в ответ пришел json
$I->seeResponseContainsJson(); // проверить что пришедший в ответ json содержит определенную структуру

$I->haveSoapHeader(); // Отравленный запрос будет содержать следующие заголовки
$I->sendSoapRequest() // отправка soap запроса
$I->seeResponseIsXml(); // в ответе пришел xml
$I->seeSoapResponseContainsStructure(); // в ответе пришла определенная структура
$I->seeXmlResponseIncludes() // пришедший в ответ xml содержит следующие поля
```

[Про тестирование апи](https://codeception.com/docs/APITesting)

## 5. Приемочное тестирование

Приемочное тестирование – это комплексное тестирование, необходимое для определения уровня готовности системы к
последующей эксплуатации. Тестирование проводится на основании набора тестовых сценариев, покрывающих основные
бизнес-операции системы.
Как правило, данный вид тестирования реализуется конечными пользователями системы, однако привлечение опытных
тестировщиков сократит время на подготовку к тестированию и позволит повысить качество и надежность проводимых
испытаний.

Простой пример приемочного тестирования

```php
$I->amOnPage('/login');
$I->fillField('username', 'davert');
$I->fillField('password', 'qwerty');
$I->click('LOGIN');
$I->see('Welcome, Davert!');
```

Этот тест можно выполнить с помощью PhpBrowser или WebDriver.

**Описание возможностей расширений для тестирования**

|                                                     |                       PhpBrowser                       |                          WebDriver                           |
|:---------------------------------------------------:|:------------------------------------------------------:|:------------------------------------------------------------:|
|                   Движок браузера                   |              Guzzle + Symfony BrowserKit               |                      Chrome or Firefox                       |
|                     JavaScript                      |                          Нет                           |                              Да                              |
|                   see/seeElement                    | Проверяет, присутствует ли текст в исходном коде HTML  |      Проверяет, текст на самом деле виден пользователю       |
| Доступ к заголовкам ответов HTTP и кодам состояния  |                           Да                           |                             Нет                              |
|                Системные требования                 |                 PHP с расширением curl                 | Chrome or Firefox; Опционально с Selenium Standalone Server  |
|                      Скорость                       |                        Быстрая                         |                          Медленная                           |

Пример теста из базового шаблона yii2

```php
    public function _before(\AcceptanceTester $I)
    {
        $I->amOnPage('/site/contact');
    }
    
    public function contactPageWorks(AcceptanceTester $I)
    {
        $I->wantTo('ensure that contact page works');
        $I->see('Contact', 'h1');
    }

    public function contactFormCanBeSubmitted(AcceptanceTester $I)
    {
        $I->amGoingTo('submit contact form with correct data');
        $I->fillField('#contactform-name', 'tester');
        $I->fillField('#contactform-email', 'tester@example.com');
        $I->fillField('#contactform-subject', 'test subject');
        $I->fillField('#contactform-body', 'test content');
        $I->fillField('#contactform-verifycode', 'testme');

        $I->click('contact-button');
        
        $I->wait(2); // wait for button to be clicked

        $I->dontSeeElement('#contact-form');
        $I->see('Thank you for contacting us. We will respond to you as soon as possible.');
    }
```

Приемочное тестирование будем производить с применением selenoid от компании aerokube так как он работает нормально с
javascript, позволяет подключаться к контейнеру из браузера, записывать видео о прохождении тестов

Пример конфигурации для запуска тестов на [selenoid](testing/Приемочное тестирование/Selenoid.md)

Пример теста из базового шаблона yii2

```php
    public function _before(\AcceptanceTester $I)
    {
        $I->amOnPage('/site/contact');
    }
    
    public function contactPageWorks(AcceptanceTester $I)
    {
        $I->wantTo('ensure that contact page works');
        $I->see('Contact', 'h1');
    }

    public function contactFormCanBeSubmitted(AcceptanceTester $I)
    {
        $I->amGoingTo('submit contact form with correct data');
        $I->fillField('#contactform-name', 'tester');
        $I->fillField('#contactform-email', 'tester@example.com');
        $I->fillField('#contactform-subject', 'test subject');
        $I->fillField('#contactform-body', 'test content');
        $I->fillField('#contactform-verifycode', 'testme');

        $I->click('contact-button');
        
        $I->wait(2); // wait for button to be clicked

        $I->dontSeeElement('#contact-form');
        $I->see('Thank you for contacting us. We will respond to you as soon as possible.');
    }
```

[Про приемочное тестирование](https://codeception.com/docs/AcceptanceTests)

### 5.1 Smoke тесты

Smoke тесты в тестировании программного обеспечения означает минимальный набор тестов на явные ошибки.
Ироничное название этого вида тестирования происходит от быстрого способа проверки инженерами электроприборов: если при
включении в розетку пошел дым – прибор требует доработки. Цель такого тестирования – проверить, что после очередной
сборки программного продукта нет явных грубых дефектов.

На самом деле от приемочного тестирования он отличается только тем что глубоко не проверяет всю работу сайта

Пример простого smoke теста

```php
$I->amOnPage('/site/contact');
$I->see('Contact', 'h1');
```

## 6. Mock

Mock - заглушка при вызове класса, метода или стороннего сервиса.

Иногда нужно сделать видимость вызова какой-то функции и для этого нужно вызвать:

```php
$user = $this->make('User', ['save' => function () { return true; }]);
```

что заменит реализацию метода save в классе юзер и отдаст в результате true при его вызове.

Так же можно заменить вызов стороннего сервиса

В общих чертах производится запрос к сервису, мок его перехватывает и отдает заранее сформированный ответ.

Для примера я буду использовать phiremock.

Добавить в api(functional, acceptance).suite.yml

```yaml
modules:
  enabled:
    - Phiremock:
        host: 127.0.0.1
        port: 8081
```

Где:
> host - ip-сервера который будет перехватывать phiremock
>
> port - порт сервера который будет перехватывать phiremock

Успешный ответ от сервиса:

```php
        use Mcustiel\Phiremock\Client\Phiremock;
        use Mcustiel\Phiremock\Client\Utils\{A, Is, Respond};
        use yii\helpers\Json;
        
        $body = Json::encode([
                "Process" => "passwordRecoveryAgent",
                "Phone" => "88002000600",
                "CallId" => "7749",
                "BaseResponse" => [
                    "ResponseMessage" => "Success"
                ]
            ]);
        $isEqualMethod = Is::equalTo('/rest/v1/eds/CheckEds');
        $isEqualJson = Is::equalTo('application/json');
        $post = A::postRequest()
            ->andUrl($isEqualMethod)
            ->andHeader('content-type', $isEqualJson)
            ->andHeader('accept', $isEqualJson);
        $respond = Respond::withStatusCode(200)->andBody($body);
        $phiremock = Phiremock::on($post)->then($respond);
        $I->expectARequestToRemoteServiceWithAResponse($phiremock);
```

Можно подменить ответ, заголовки, перехватывать определенные эндпоинты

О мокерах сторонних сервисов:

[phiremock](https://github.com/mcustiel/phiremock-codeception-extension)

[http-mock](https://github.com/mcustiel/codeception-http-mock)

[wiremock](https://github.com/mcustiel/codeception-wiremock-extension)

## 7. Фикстуры

Фикстуры это заранее сгенерированные наборы данных для тестов. Это могут быть как массивы, так и записи в базе данных.

Иногда для тестов нужны данные для проверки и фикстуры по заранее обозначенному шаблону могут собрать любое количество
наборов данных.

Для работы необходимо настроенное консольное приложение yii2.

[Пример конфига](testing/Фикстуры/Конфиги.md)

Пример шаблона для фикстуры:

```php
/**
 * @var $faker \Faker\Generator
 * @var $index integer
 */
return [
    'AgentID' => $index+1,
    'Login' => '88002000600',
    'Password' => sha1('password'),
    'CreateDate' => date('Y-m-d H:i:s'),
    'UpdateDate' => date('Y-m-d H:i:s'),
    'LastAuthorization' => date('Y-m-d H:i:s'),
    'ConfirmEmail' => $faker->boolean(),
    'ActivityFlag' => $faker->boolean(),
];
```

Пример сгенерированных данных:

```php
return [
    'AgentAccount0' => [
        'AgentID' => 1,
        'Login' => '88002000600',
        'Password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'CreateDate' => '2022-06-08 08:40:40',
        'UpdateDate' => '2022-06-08 08:40:40',
        'LastAuthorization' => '2022-06-08 08:40:40',
        'ConfirmEmail' => 1,
        'ActivityFlag' => 1,
    ],
    'AgentAccount1' => [
        'AgentID' => 2,
        'Login' => '88002000600',
        'Password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'CreateDate' => '2022-06-08 08:40:40',
        'UpdateDate' => '2022-06-08 08:40:40',
        'LastAuthorization' => '2022-06-08 08:40:40',
        'ConfirmEmail' => 1,
        'ActivityFlag' => 1,
    ],
    'AgentAccount2' => [
        'AgentID' => 3,
        'Login' => '88002000600',
        'Password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'CreateDate' => '2022-06-08 08:40:40',
        'UpdateDate' => '2022-06-08 08:40:40',
        'LastAuthorization' => '2022-06-08 08:40:40',
        'ConfirmEmail' => 1,
        'ActivityFlag' => 1,
    ],
    'AgentAccount3' => [
        'AgentID' => 4,
        'Login' => '88002000600',
        'Password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'CreateDate' => '2022-06-08 08:40:40',
        'UpdateDate' => '2022-06-08 08:40:40',
        'LastAuthorization' => '2022-06-08 08:40:40',
        'ConfirmEmail' => 1,
        'ActivityFlag' => 1,
    ],
    'AgentAccount4' => [
        'AgentID' => 5,
        'Login' => '88002000600',
        'Password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'CreateDate' => '2022-06-08 08:40:40',
        'UpdateDate' => '2022-06-08 08:40:40',
        'LastAuthorization' => '2022-06-08 08:40:40',
        'ConfirmEmail' => 1,
        'ActivityFlag' => 1,
    ],
    'AgentAccount5' => [
        'AgentID' => 6,
        'Login' => '88002000600',
        'Password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'CreateDate' => '2022-06-08 08:40:40',
        'UpdateDate' => '2022-06-08 08:40:40',
        'LastAuthorization' => '2022-06-08 08:40:40',
        'ConfirmEmail' => 1,
        'ActivityFlag' => 1,
    ],
    'AgentAccount6' => [
        'AgentID' => 7,
        'Login' => '88002000600',
        'Password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'CreateDate' => '2022-06-08 08:40:40',
        'UpdateDate' => '2022-06-08 08:40:40',
        'LastAuthorization' => '2022-06-08 08:40:40',
        'ConfirmEmail' => 1,
        'ActivityFlag' => 1,
    ],
    'AgentAccount7' => [
        'AgentID' => 8,
        'Login' => '88002000600',
        'Password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'CreateDate' => '2022-06-08 08:40:40',
        'UpdateDate' => '2022-06-08 08:40:40',
        'LastAuthorization' => '2022-06-08 08:40:40',
        'ConfirmEmail' => 1,
        'ActivityFlag' => 1,
    ],
    'AgentAccount8' => [
        'AgentID' => 9,
        'Login' => '88002000600',
        'Password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'CreateDate' => '2022-06-08 08:40:40',
        'UpdateDate' => '2022-06-08 08:40:40',
        'LastAuthorization' => '2022-06-08 08:40:40',
        'ConfirmEmail' => 1,
        'ActivityFlag' => 1,
    ],
    'AgentAccount9' => [
        'AgentID' => 10,
        'Login' => '88002000600',
        'Password' => '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8',
        'CreateDate' => '2022-06-08 08:40:40',
        'UpdateDate' => '2022-06-08 08:40:40',
        'LastAuthorization' => '2022-06-08 08:40:40',
        'ConfirmEmail' => 1,
        'ActivityFlag' => 1,
    ],
];
```

Пример загрузки и получения данных из фикстуры

```php
// Загрузка фикстур
$this->tester->haveFixtures([
    'user' => [
        'class' => UserFixture::class,
        'dataFile' => codecept_data_dir() . 'user.php'
    ]
]);
// получить первого пользователя из фикстур
$this->tester->grabFixture('user', 0);
```

[О фикстурах на codeception](https://codeception.com/docs/reference/Fixtures)

[О фикстурах в yii2](https://www.yiiframework.com/doc/guide/2.0/ru/test-fixtures)

## 8. Отчеты

Что это?

Для чего это?

[Покрытие тестами](testing/Отчеты/покрытие.md)

[Простой отчет](testing/Отчеты/report.xml.md)

[Генератор отчетов: Allure framework](testing/Отчеты/Allure framework.md)

[Генератор отчетов: TESTOMAT.IO](testing/Отчеты/Allure framework.md)

## 9. Лучшие практики

Login

Рекомендуется поместить широко используемые действия в класс Actor. Хорошим примером является действие по входу в
систему, которое, вероятно, будет активно участвовать в принятии или функциональном тестировании:

```php
class AcceptanceTester extends \Codeception\Actor
{
    // do not ever remove this line!
    use _generated\AcceptanceTesterActions;

    public function login($name, $password)
    {
        $I = $this;
        $I->amOnPage('/login');
        $I->submitForm('#loginForm', [
            'login' => $name,
            'password' => $password
        ]);
        $I->see($name, '.navbar');
    }
}
```

Теперь вы можете использовать метод входа в систему в своих тестах:
```php
// $I is AcceptanceTester
$I->login('miles', '123456');
```