Allure Framework — это гибкий и легкий многоязычный инструмент для составления отчетов о тестировании, который не только
показывает очень сжатое представление того, что было протестировано в удобной форме веб-отчета, но и позволяет всем, кто
участвует в процессе разработки, извлекать максимум полезной информации из повседневного выполнения тестов.

![img](https://docs.qameta.io/allure/images/get_started_report_overview.png "отчет allure")

Для работы с данным отчетом нужно добавить его как компонент composer проекта

```json
{
  "require": {
    "php": "7.4",
    "allure-framework/allure-codeception": "1.5.2"
  }
}
```

и в codeception.yml следующий модуль:

```yaml
extensions:
  enabled:
    - Yandex\Allure\Codeception\AllureCodeception
  config:
    Yandex\Allure\Codeception\AllureCodeception:
      deletePreviousResults: true
      outputDirectory: allure-results
```

Отчет будет генерироваться автоматически при запуске тестов.

Структура отчета

Каждый отчет Allure поддерживается древовидной структурой данных, которая представляет собой процесс выполнения теста.
Различные вкладки позволяют переключаться между представлениями исходной структуры данных, что дает другую перспективу.
Обратите внимание, что все древовидные представления, включая Behaviors, Categories, xUnit и Packages, поддерживают
фильтрацию и сортировку.

Обзорная страница

![img](https://docs.qameta.io/allure/images/tab_overview.png "обзорная страница allure")

Категории

![img](https://docs.qameta.io/allure/images/tab_categories.png "категории allure")

Наборы тестов

![img](https://docs.qameta.io/allure/images/tab_suites.png "наборы тестов allure")

Графики

![img](https://docs.qameta.io/allure/images/tab_graphs.png "графики allure")