report.xml

Простой отчет это xml файл формата:
```xml
<?xml version="1.0" encoding="UTF-8"?>
<testsuites>
  <testsuite name="acceptance" tests="5" assertions="9" errors="0" failures="0" skipped="0" time="19.560249">
    <testcase file="/var/www/basic/tests/acceptance/AboutCest.php" name="ensureThatAboutWorks" class="AboutCest" feature="ensure that about works" assertions="1" time="10.515658"/>
    <testcase file="/var/www/basic/tests/acceptance/ContactCest.php" name="contactPageWorks" class="ContactCest" feature="ensure that contact page works" assertions="1" time="0.567992"/>
    <testcase file="/var/www/basic/tests/acceptance/ContactCest.php" name="contactFormCanBeSubmitted" class="ContactCest" feature="contact form can be submitted" assertions="2" time="3.295544"/>
    <testcase file="/var/www/basic/tests/acceptance/HomeCest.php" name="ensureThatHomePageWorks" class="HomeCest" feature="ensure that home page works" assertions="3" time="2.548188"/>
    <testcase file="/var/www/basic/tests/acceptance/LoginCest.php" name="ensureThatLoginWorks" class="LoginCest" feature="ensure that login works" assertions="2" time="2.632867"/>
  </testsuite>
</testsuites>
```

Он используется к примеру при формировании отчета в gitlab ci/cd

Для создания достаточно в строку запуска тестов добавить параметр --xml