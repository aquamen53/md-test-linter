Покрытие тестами (coverage)
![img](https://codeception.com/images/coverage.png "покрытие тестами")

Для покрытия тестами необходимо расширение для php - xdebug.

Его нужно настроить так что бы его параметр mode содержал coverage и в конфиге codeception.yml добавить:

```yaml
coverage:
  enabled: true
  whitelist:
    include:
      - models/forms/*
      - helpers/*
      - components/*
```

где include список папок и дректорий которые будут проверяться на покрытие тестами.

Для запуска тестов с покрытием необходимо выполнить следующую команду:

```bash
vendor/bin/codecept run --coverage --coverage-xml --coverage-html -c vendor/migcredit/_название_сервиса_/src/
```


где:

> --coverage - само покрытие
> --coverage-xml - сгенерировать отчет в формате xml
> --coverage-html - сгенерировать отчет в виде html страниц


Результаты покрытия попадут в папку tests/output