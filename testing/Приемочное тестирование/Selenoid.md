## Для запуска тестов мы будем использовать selenoid компании aerokube, который будем запускать из docker контейнера

[Сайт selenoid от aerokube](https://aerokube.com/selenoid/latest/)

Пример конфигурации docker-compose файла

```yaml
version: '3.3'
services:
  traefik:
    image: "traefik:latest"
    container_name: "traefik"
    command:
      - "--api.insecure=true"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--entrypoints.web.address=:80"
    ports:
      - "80:80"
      - "8080:8080"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock:ro"
    networks:
      - traefik-public
    restart: always

  nginx:
    build: ./images/nginx/
    container_name: nginx
    volumes:
      - ./logs/nginx:/var/log/nginx/
      - ../:/var/www/basic/
      - ./configs/nginx/nginx.conf:/etc/nginx/nginx.conf
      - ./configs/nginx/localhost.conf:/etc/nginx/sites-enabled/localhost.com.conf
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.localhost-test.rule=Host(`localhost-test.com`)"
      - "traefik.http.routers.localhost-test.entrypoints=web"
      - "traefik.http.routers.localhost-test.service=localhost-test-web-svc"
      - "traefik.http.services.localhost-test-web-svc.loadbalancer.server.port=80"
    links:
      - php
    depends_on:
      - traefik
    restart: always
    networks:
      - traefik-public

  php:
    build: ./images/php7.4/
    container_name: php
    volumes:
      - ../:/var/www/basic/
      - ./configs/php/ImageMagick-6/policy.xml:/etc/ImageMagick-6/policy.xml
      - ./configs/php/php.ini:/etc/php/7.4/fpm/php.ini
      - ./configs/php/php_console.ini:/etc/php/7.4/cli/php.ini
    restart: always
    networks:
      - traefik-public

  selenoid:
    image: "aerokube/selenoid"
    volumes:
      - ./configs/selenoid/:/etc/selenoid
      - /var/run/docker.sock:/var/run/docker.sock
      - ./video:/opt/selenoid/video
      - ./logs/selenoid:/opt/selenoid/logs
    environment:
      - OVERRIDE_VIDEO_OUTPUT_DIR=$PWD/video
      - TZ=Europe/Moscow
    command: [ "-conf", "/etc/selenoid/browsers.json", "-video-output-dir", "/opt/selenoid/video", "-log-output-dir", "/opt/selenoid/logs", "-container-network", "traefik-public" ]
    ports:
      - "4444:4444"
    networks:
      - traefik-public

  selenoid-ui:
    image: "aerokube/selenoid-ui"
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.selenoid-ui.rule=Host(`selenoid-ui.loc`)"
      - "traefik.http.routers.selenoid-ui.entrypoints=web"
      - "traefik.http.routers.selenoid-ui.service=selenoid-ui-web-svc"
      - "traefik.http.services.selenoid-ui-web-svc.loadbalancer.server.port=8080"
    command: [ "--selenoid-uri", "http://selenoid:4444" ]
    depends_on:
      - selenoid
    networks:
      - traefik-public

networks:
  traefik-public:
    external: true

```

Он включает в себя traefik для проксирования портов, контейнер с nginx, php7.4 и selenoid+selenoid-ui

### Обязательно в блоке с command нужно ещё раз указывать сеть которой связаны контейнеры `"-container-network", "traefik-public"`

Данный конфиг позволяет записывать видео о прохождении тестов по пути docker/video, а так же запись логов по пути
docker/logs/selenoid

Так же необходимо завести конфиг browser.json с запускаемыми браузерами по пути docker/configs/selenoid со следующим
содержимым

```json
{
  "firefox": {
    "default": "96.0",
    "versions": {
      "96.0": {
        "image": "selenoid/vnc_firefox:96.0",
        "port": "4444",
        "path": "/wd/hub"
      }
    }
  },
  "chrome": {
    "default": "90.0",
    "versions": {
      "90.0": {
        "image": "selenoid/chrome:90.0",
        "env": [
          "LANG=ru_RU.UTF-8",
          "LANGUAGE=ru:en",
          "LC_ALL=ru_RU.UTF-8"
        ],
        "port": "4444",
        "path": "/"
      }
    }
  }
}
```

Сюда добавляются конфиги браузеров с разными версиями

Далее необходимо скачать следующие docker-контейнеры перед запуском тестов

```bash
docker pull selenoid/vnc_firefox:96.0

docker pull selenoid/chrome:90.0

docker pull selenoid/video-recorder:latest-release
```

В нашем случае в конфиге указан образ selenoid/vnc_firefox:96.0 его и скачиваем и второй с записью видео

Так же необходимо настроить конфиг acceptance.suite.yml со следующим содержимым

```yaml
class_name: AcceptanceTester
modules:
  enabled:
    - WebDriver:
        url: 'http://nginx'
        host: 'selenoid'
        browser: firefox
        capabilities:
          enableVideo: true
          videoScreenSize: "1920x1080"
          videoFrameRate: 24
          enableLog: true
          enableVNC: true
    - Yii2:
        part: orm
        entryScript: index-test.php
        cleanup: false
```

Отсюда можно включать и отключать доступ к контейнеру с веб-морды selenoid-ui, разрешение экрана, запись видео и логов.

Готовые конфиги отдельно приложу для yii2 basic

Посмотреть работу сервиса можно по [адресу](https://selenoid-ui.loc)