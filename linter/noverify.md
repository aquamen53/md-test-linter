### NoVerify

NoVerify - это PHP linter: он находит возможные ошибки и нарушения стиля в вашем коде.

* NoVerify не имеет конфигурации: любая сообщенная проблема в PHPDoc или PHP-коде должна быть исправлена.
* NoVerify стремится понять PHP-код, по крайней мере, так же хорошо, как PHPStorm. Если он ведет себя неправильно или
  оптимально, пожалуйста, сообщите о проблеме.
* Этот инструмент написан на Go и использует форк z7zmey/php-parser.

### Особенности

1. Быстро: анализ ~100 тыс. LOC/с (строк кода в секунду) на Core i7;
2. Инкрементный: может анализировать изменения в git и показывать только новые отчеты. Скорость индексации составляет ~
   1M LOC/s;
3. Автоисправки некоторых предупреждений;
4. Поддержка PHP 7 и PHP 8;
5. Поддержка различий и базовых режимов.

NoVerify по умолчанию имеет следующие проверки:

* Недоступный код
* Слишком мало аргументов при вызове функции/метода
* Вызов неопределенной функции/метода
* Получение неопределенного свойства константы/класса
* Класс не найден
* PHPDoc неверен
* Неопределенная переменная
* Переменная не всегда определена
* Случай без перерыва;
* Синтаксическая ошибка
* Неиспользуемая переменная
* Неправильный доступ к private/protected элементам
* Неправильная реализация интерфейса IteratorAggregate
* Неправильное определение массива, например, дубликаты ключей

### Установка

Выполните следующую команду:
```shell
composer require --dev vkcom/noverify
```

После того, как NoVerify установлен как зависимость, выполните следующую команду, чтобы загрузить двоичный файл.

```shell
./vendor/bin/noverify-get
```

Готовый к запуску двоичный файл будет помещен в ту же папку и может быть запущен со следующей командой:

```shell
./vendor/bin/noverify
```

### Использование

```shell
noverify check ./lib
```

Это приведет к ошибкам:

```shell
...
<critical> WARNING strictCmp: Non-strict string comparison (use ===) at swiftmailer/lib/classes/Swift/Signers/DomainKeySigner.php:417
        $nofws = ('nofws' == $this->canon);
                  ^^^^^^^^^^^^^^^^^^^^^^^
<critical> WARNING parentConstructor: Missing parent::__construct() call at swiftmailer/lib/classes/Swift/Attachment.php:27
    public function __construct($data = null, $filename = null, $contentType = null)
                    ^^^^^^^^^^^
2021/07/08 16:13:19 Found 113 critical and 10 minor reports
```

[Статья на хабре](https://habr.com/ru/company/vk/blog/442284/)
[Статья на гит](https://github.com/VKCOM/noverify)